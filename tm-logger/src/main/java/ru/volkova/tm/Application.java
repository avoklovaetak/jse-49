package ru.volkova.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.service.IActiveMQConnectionService;
import ru.volkova.tm.listener.LogMessageListener;
import ru.volkova.tm.service.ActiveMQConnectionService;

public class Application {

    @SneakyThrows
    public static void main(final String[] args) {
        @NotNull final IActiveMQConnectionService receiverService = new ActiveMQConnectionService();
        receiverService.receive(new LogMessageListener());
    }

}
