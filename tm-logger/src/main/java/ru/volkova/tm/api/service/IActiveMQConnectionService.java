package ru.volkova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.jms.MessageListener;

public interface IActiveMQConnectionService {

    @SneakyThrows
    void receive(@NotNull final MessageListener listener);

}
