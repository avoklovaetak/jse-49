package ru.volkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.Session;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.model.UserGraph;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @WebMethod
    void addUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "entity", partName = "entity") UserGraph entity
    );

    @WebMethod
    void clearUsers(@NotNull @WebParam(name = "session", partName = "session") Session session);


    @WebMethod
    void createUserByLogPass(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password
    );

    @WebMethod
    void createUserWithEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "email", partName = "email") String email
    );

    @WebMethod
    void createUserWithRole(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "role", partName = "role") Role role
    );

    @NotNull
    @WebMethod
    List<User> findAllUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    );

    @Nullable
    @WebMethod
    User findUserById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @Nullable
    @WebMethod
    User findUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    );

    @WebMethod
    boolean isEmailExists(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    );

    @WebMethod
    boolean isLoginExists(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    );

    @WebMethod
    void lockByEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    );

    @WebMethod
    void lockById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    void lockByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    );

    @WebMethod
    void removeUserByEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    );

    @WebMethod
    void removeUserById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    void removeUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    );


    @WebMethod
    void unlockUserByEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    );

    @WebMethod
    void unlockUserById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    void unlockUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    );

    @WebMethod
    void updateUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "secondName", partName = "secondName") String secondName,
            @Nullable @WebParam(name = "middleName", partName = "middleName") String middleName
    );

}
