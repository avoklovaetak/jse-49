package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IExecutorService {

    void submit(@NotNull final Runnable runnable);

}
