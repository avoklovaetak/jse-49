package ru.volkova.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.model.IProjectRepositoryGraph;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.model.ProjectGraph;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectRepositoryGraph extends AbstractRepositoryGraph<ProjectGraph> implements IProjectRepositoryGraph {

    public ProjectRepositoryGraph(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @Nullable
    public ProjectGraph insert(@Nullable ProjectGraph projectGraph) {
        if (projectGraph == null) throw new ProjectNotFoundException();
        entityManager.persist(projectGraph);
        return projectGraph;
    }

    @Override
    public void clear(@NotNull String userId) {
        entityManager
                .createQuery("DELETE FROM project t WHERE t.user.id = :userId")
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true);
    }

    @Override
    @NotNull
    public List<ProjectGraph> findAll(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT t FROM project t", ProjectGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    @Nullable
    public ProjectGraph findById(@NotNull String userId, @NotNull String id) {
        return entityManager
                .createQuery("SELECT t FROM project t WHERE t.user.id = :userId AND t.id = :id",
                        ProjectGraph.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public ProjectGraph findOneByIndex(@NotNull String userId, @NotNull Integer index) {
        return entityManager
                .createQuery("SELECT t FROM project t WHERE t.user.id = :userId",
                        ProjectGraph.class)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public ProjectGraph findOneByName(@NotNull String userId, @NotNull String name) {
        return entityManager
                .createQuery("SELECT t FROM project t WHERE t.user.id = :userId AND t.name = :name",
                        ProjectGraph.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void changeOneStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status) {
        entityManager
                .createQuery("UPDATE project t SET t.status = :status" +
                                "WHERE t.user.id = :userId AND t.id = :id",
                        ProjectGraph.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setParameter("status", status)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void changeOneStatusByName(@NotNull String userId, @Nullable String name, @Nullable Status status) {
        entityManager
                .createQuery("UPDATE project t SET t.status = :status" +
                                "WHERE t.user.id = :userId AND t.name = :name ",
                        ProjectGraph.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setParameter("status", status)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        entityManager
                .createQuery("DELETE FROM project t WHERE t.user.id = :userId and t.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void removeOneByName(@NotNull String userId, @Nullable String name) {
        entityManager
                .createQuery("DELETE FROM project t WHERE t.user.id = :userId and t.name = :name")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void updateOneById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        entityManager
                .createQuery("UPDATE project t SET t.name = :name, t.description = :description" +
                                "WHERE t.user.id = :user_id AND t.id = :id",
                        ProjectGraph.class)
                .setParameter("name", name)
                .setParameter("description", description)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

}
