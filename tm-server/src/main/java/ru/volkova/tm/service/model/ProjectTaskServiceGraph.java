package ru.volkova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.model.IProjectTaskServiceGraph;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.model.TaskGraph;
import ru.volkova.tm.repository.model.ProjectRepositoryGraph;
import ru.volkova.tm.repository.model.TaskRepositoryGraph;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectTaskServiceGraph implements IProjectTaskServiceGraph {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskServiceGraph(
            @NotNull final IConnectionService connectionService
    ) {
        this.connectionService = connectionService;
    }

    @Override
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraph taskRepository = new TaskRepositoryGraph(entityManager) ;
            taskRepository.bindTaskByProjectId(userId, projectId, taskId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskGraph> findAllTasksByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraph taskRepository = new TaskRepositoryGraph(entityManager) ;
            final List<TaskGraph> taskGraphs = taskRepository.findAllByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
            return taskGraphs;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(
            @NotNull final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            @NotNull final TaskRepositoryGraph taskRepository = new TaskRepositoryGraph(entityManager) ;
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeById(userId,projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraph taskRepository = new TaskRepositoryGraph(entityManager) ;
            taskRepository.unbindTaskByProjectId(userId, projectId, taskId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
