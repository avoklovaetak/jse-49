package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.dto.IProjectTaskService;
import ru.volkova.tm.api.service.dto.ITaskService;
import ru.volkova.tm.dto.Project;
import ru.volkova.tm.dto.Task;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.marker.UnitCategory;
import ru.volkova.tm.service.dto.ProjectTaskService;
import ru.volkova.tm.service.dto.TaskService;

import java.util.List;

public class ProjectGraphTaskGraphServiceTest {

    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    private final User user = new User();

    @Test
    @Category(UnitCategory.class)
    public void bindTaskByProjectIdTest() {
        final Project project = new Project();
        final Task task = new Task();
        task.setUser(user);
        taskService.insert(task);
        projectTaskService.bindTaskByProjectId(task.getUser().getId(), project.getId(), task.getId());
        Assert.assertNotNull(task.getProject().getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllByProjectIdTest() {
        final Project project = new Project();
        project.setUser(user);
        final Task task1 = new Task();
        final Task task2 = new Task();
        task1.setUser(user);
        task2.setUser(user);
        taskService.insert(task1);
        taskService.insert(task2);
        projectTaskService.bindTaskByProjectId(user.getId(), project.getId(), task1.getId());
        projectTaskService.bindTaskByProjectId(user.getId(), project.getId(), task2.getId());
        List<Task> taskList = projectTaskService.findAllTasksByProjectId(project.getUser().getId(), project.getId());
        Assert.assertNotNull(taskList.contains(task1));
        Assert.assertTrue(taskList.contains(task2));
    }

    @Test
    @Category(UnitCategory.class)
    public void unbindTaskByProjectIdTest() {
        final Project project = new Project();
        final Task task = new Task();
        task.setUser(user);
        taskService.insert(task);
        projectTaskService.bindTaskByProjectId(task.getUser().getId(), project.getId(), task.getId());
        projectTaskService.unbindTaskByProjectId(task.getUser().getId(), project.getId(), task.getId());
        Assert.assertNull(task.getProject().getId());
    }

}
