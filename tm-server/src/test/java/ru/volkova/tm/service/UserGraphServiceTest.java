package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.api.service.dto.IAdminUserService;
import ru.volkova.tm.api.service.dto.IUserService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.marker.UnitCategory;

public class UserGraphServiceTest {

    @NotNull
    private final ServiceLocator serviceLocator = new Bootstrap();

    private final IPropertyService propertyService = serviceLocator.getPropertyService();

    private final IConnectionService connectionService = serviceLocator.getConnectionService();

    private final IAdminUserService adminUserService = serviceLocator.getAdminUserDTOService();

    private final IUserService userService = serviceLocator.getUserDTOService();

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final User user = new User();
        Assert.assertNotNull(adminUserService.add(user));
    }

}
